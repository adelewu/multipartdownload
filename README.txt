This is simple testing project. It's a prototype of multi-part download performance. However, the testing project does not include the following functions.
1. Pause/resume download
2. Handle network reconnect case
3. Allow tester to decide saving path
This project could only present the difference of Android DownloadManager and multi-part download.